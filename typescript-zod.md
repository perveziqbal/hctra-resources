## Typescript

1. [Total Typescript](https://www.totaltypescript.com/tutorials/beginners-typescript)
2. [Master assertion functions](https://www.youtube.com/watch?v=8lM609lci7E)
3. [Master function overloads](https://www.youtube.com/watch?v=D1a8OoBWi1g)
4. [Typescript Utility Types](https://www.youtube.com/watch?v=EU0TB_8KHpY)
5. [Advanced Typescript Fundamentals](https://egghead.io/courses/advanced-typescript-fundamentals-579c174f)
6. [Advanced Static Types in Typescript](https://egghead.io/courses/advanced-static-types-in-typescript)

## Zod

1. [When should you use Zod](https://www.totaltypescript.com/when-should-you-use-zod)
2. [Zod](https://www.totaltypescript.com/tutorials/zod) course

## React

1. [Managing State](https://react.dev/learn/managing-state)
2. [React Router](https://reactrouter.com/en/main/start/tutorial)
3. [React Query](https://tkdodo.eu/blog/practical-react-query)

## Asynchrony

1. [Javascript Promises in Depth](https://egghead.io/courses/javascript-promises-in-depth)
2. [async/await](https://egghead.io/courses/asynchronous-javascript-with-async-await)

## Blogs

1. [Migrate from CRA to vite](https://www.robinwieruch.de/vite-create-react-app/)
2. [Fully typed web apps](https://www.epicweb.dev/fully-typed-web-apps)
3. [Static vs unit vs integration vs e2e tests](https://kentcdodds.com/blog/static-vs-unit-vs-integration-vs-e2e-tests)
4. [Testing trophy and testing classifications](https://kentcdodds.com/blog/the-testing-trophy-and-testing-classifications)
